import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FlightfromComponent } from './flightfrom/flightfrom.component'

const routes: Routes = [
  {path:'flights' , component: FlightfromComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
