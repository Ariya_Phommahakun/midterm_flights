import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerModule} from '@angular/material/datepicker'
import { PageService } from '../share/page.service';
import { FlightModel } from './flight-model';

@Component({
  selector: 'app-flightfrom',
  templateUrl: './flightfrom.component.html',
  styleUrls: ['./flightfrom.component.css']
})
export class FlightfromComponent implements OnInit {
  flight !: FlightModel[]
  flightform !: FormGroup
  startDate: Date
  constructor(private form: FormBuilder,public page: PageService) {
    this.startDate = new Date(Date.now())
    this.flightform = this.form.group({
      fullName: ['',Validators.required] ,
      from: [null , Validators.required] ,
      to: [null , Validators.required] ,
      type: ['' , Validators.required] ,
      departure: ['' , Validators.required],
      arrival: [''],
      adults: [0 , [Validators.required , Validators.max(10) , Validators.pattern('[0-9]*$')]],
      children: [0 , [ Validators.max(10) , Validators.pattern('[0-9]*$')]],
      infants: [0 , [ Validators.max(10) , Validators.pattern('[0-9]*$')]]
    })
  }

  ngOnInit(): void {
    this.getPages()
  }

  getPages(){
    this.flight = this.page.getPages()
  }

  onSubmit(f:FlightModel): void{
    if(f.from == f.to) return alert('ขออภัย จุดเริ่มต้นและจุดหมายจะต้องไม่เหมือนกันค่ะ')

    const yearDeparture = f.departure.getFullYear() + 543
    const yearArrival = f.arrival.getFullYear() + 543

    f.departure = new Date((f.departure.getMonth() + 1) + '/' + f.departure.getDate() + "/" + yearDeparture)
    f.arrival = new Date((f.arrival.getMonth() + 1) + '/' + f.arrival.getDate() + "/" + yearArrival)

    this.page.addFlight(f)
  }

}
