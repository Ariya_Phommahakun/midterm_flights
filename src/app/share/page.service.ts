import { Injectable } from '@angular/core';
import { Mock } from './mock';
import { FlightModel } from '../flightfrom/flight-model'

@Injectable({
  providedIn: 'root'
})
export class PageService {
  flights: FlightModel[] = []
  constructor() {
    this.flights = Mock.testData
  }

  getPages(){
    return this.flights
  }

  addFlight(f:FlightModel){
    this.flights.push(f)
  }
}
