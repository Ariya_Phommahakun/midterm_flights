import { FlightModel } from '../flightfrom/flight-model'
export class Mock {
  public static testData: FlightModel[] = [
    {
      fullName: "ARIYA PHOMMAHAKUN",
      from: "Thailand",
      to: "Korea",
      type: "One way",
      departure: new Date('03-15-2565'),    //month-day-year
      arrival: new Date('03-17-2565'),      //month-day-year
      adults: 2,
      children: 1,
      infants: 1
    }
  ]
}
